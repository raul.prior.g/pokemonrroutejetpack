package com.priorDev.pokerroutejc.domain.pokemon.useCases

data class PokemonUseCases(
    val getWeaknessesAndStrengths: GetWeaknessesAndStrengths
)
