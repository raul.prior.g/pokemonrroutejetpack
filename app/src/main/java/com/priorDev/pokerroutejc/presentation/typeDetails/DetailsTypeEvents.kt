package com.priorDev.pokerroutejc.presentation.typeDetails

sealed class DetailsTypeEvents {
    object onDismiss : DetailsTypeEvents()
}
