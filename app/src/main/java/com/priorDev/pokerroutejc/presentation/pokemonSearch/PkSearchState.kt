package com.priorDev.pokerroutejc.presentation.pokemonSearch

data class PkSearchState(
    val searchText: String
)
