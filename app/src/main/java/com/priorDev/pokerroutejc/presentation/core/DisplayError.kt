package com.priorDev.pokerroutejc.presentation.core

enum class DisplayError {
    Dialog,
    FullScreen
}
